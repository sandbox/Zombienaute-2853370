<?php

namespace Drupal\workbench_access_state_transition;

use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Creates a service modifier to hijack the transport service.
 */
class WorkbenchAccessStateTransitionServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {

    $service_id = 'workbench_moderation.state_transition_validation';
    if ($validator = $container->getDefinition($service_id)) {
      $validator->setClass('Drupal\workbench_access_state_transition\WorkbenchAccessStateTransitionValidation');
      $validator->setArguments(
        [
          new Reference('entity_type.manager'),
          new Reference('entity.query'),
          new Reference('plugin.manager.workbench_access.scheme'),
        ]
      );
      $container->setDefinition($service_id, $validator);
    }
  }

}
