<?php

namespace Drupal\workbench_access_state_transition;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountInterface;
use Drupal\workbench_access\WorkbenchAccessManager;
use Drupal\workbench_moderation\StateTransitionValidation;

/**
 * Validates whether a certain state transition is allowed.
 */
class WorkbenchAccessStateTransitionValidation extends StateTransitionValidation {

  /**
   * The workbench access manager.
   *
   * @var \Drupal\workbench_access\WorkbenchAccessManager
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueryFactory $query_factory, WorkbenchAccessManager $manager) {
    parent::__construct($entity_type_manager, $query_factory);
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getValidTransitions(ContentEntityInterface $entity, AccountInterface $user) {
    /** @var \Drupal\workbench_access\AccessControlHierarchyInterface $scheme */
    $scheme = $this->manager->getActiveScheme();
    // If a user doesn't have edit access according to the scheme, don't let
    // them transition the node.
    if ($scheme->checkEntityAccess($entity, 'edit', $user, $this->manager)->isForbidden()) {
      return [];
    }
    $transitions = parent::getValidTransitions($entity, $user);

    return $transitions;
  }

}
