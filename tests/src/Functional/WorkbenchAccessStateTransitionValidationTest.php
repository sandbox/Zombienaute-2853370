<?php

namespace Drupal\Tests\workbench_access_state_transition\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests our custom state transition validator.
 *
 * @group workbench_access_state_transition
 */
class WorkbenchAccessStateTransitionValidationTest extends BrowserTestBase {

  /**
   * The role we are testing.
   *
   * @var string
   */
  protected $role;

  /**
   * The staff taxonomy term.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  protected $staffTerm;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'workbench_access_state_transition',
    'workbench_access',
    'workbench_moderation',
    'node',
    'taxonomy',
    'options',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $node_type = $this->createContentType(['type' => 'page']);
    $node_type->setThirdPartySetting('workbench_access', 'workbench_access_status', 1);
    $node_type->setThirdPartySetting('workbench_moderation', 'enabled', TRUE);
    $node_type->setThirdPartySetting('workbench_moderation', 'allowed_moderation_states', [
      'draft',
      'needs_review',
      'published',
    ]);
    $node_type->save();

    $entity_display = EntityViewDisplay::load('node.page.default');
    $entity_display->setComponent('workbench_moderation_control');
    $entity_display->save();

    $vocab = Vocabulary::create(['vid' => 'workbench_access', 'name' => 'Access']);
    $vocab->save();

    $this->staffTerm = Term::create([
      'vid' => $vocab->id(),
      'name' => 'Staff',
    ]);
    $this->staffTerm->save();

    // Create workbench access storage for node.
    $field_storage = FieldStorageConfig::create([
      'field_name' => WORKBENCH_ACCESS_FIELD,
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => 1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();
    // Create an instance of the workbench field on the content type.
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $node_type->id(),
      'settings' => [
        'handler' => 'default:taxonomy_term',
        'handler_settings' => [
          'target_bundles' => [
            'workbench_access' => 'workbench_access',
          ],
        ],
      ],
    ])->save();

    $config = \Drupal::configFactory()->getEditable('workbench_access.settings');
    $config->set('scheme', 'taxonomy');
    $config->set('parents', ['workbench_access' => 'workbench_access']);

    $fields['node'][$node_type->id()] = WORKBENCH_ACCESS_FIELD;
    $config->set('fields', $fields);
    $config->save();

    // This returns an rid.
    $this->role = $this->drupalCreateRoleParent([
      'administer nodes',
      'view moderation states',
      'view latest version',
      'view any unpublished content',
      'use draft_needs_review transition',
    ], 'staff');
    $user = $this->createUser();
    $user->addRole($this->role);
    $user->save();

    $this->drupalLogin($user);
  }

  /**
   * Tests the transition validation controls access to the moderation form.
   */
  public function testModerationFormDoesNotDisplayWithoutSection() {
    // Create a published node, then a draft so we have a latest version route.
    $node = $this->createNode(['moderation_state' => 'published']);
    $node->moderation_state = 'draft';
    $node->save();

    $url = $node->toUrl('latest-version');
    $this->drupalGet($url);

    $session = $this->getSession();
    $page = $session->getPage();

    // Page has moderation form.
    $this->assertTrue($page->hasSelect('Moderate'), 'Moderation form is not displaying when it should.');

    // Set node access field to Staff.
    $node->field_workbench_access = $this->staffTerm->id();
    $node->save();

    // Check the moderation form no longer displays.
    $this->drupalGet($url);
    $this->assertFalse($page->hasSelect('Moderate'), 'Moderation form is displaying when it should not.');

    // Set the access for the role to the staff term.
    \Drupal::state()->set('workbench_access_roles_' . $this->role, [$this->staffTerm->id() => 1]);
    // Check the form displays again.
    $this->drupalGet($url);
    $this->assertTrue($page->hasSelect('Moderate'), 'Moderation form is not displaying when it should.');
  }

}
