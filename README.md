# Workbench Access State Transition

Integrates workbench_access with workbench_moderation to limit a user's access to transition an entity between moderation states based on the constraints set by workbench_access. 
